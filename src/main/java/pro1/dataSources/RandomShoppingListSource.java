package pro1.dataSources;

import pro1.dataModel.ShoppingItem;
import pro1.dataModel.ShoppingList;

import java.util.ArrayList;
import java.util.Arrays;

public class RandomShoppingListSource implements ShoppingListSource {
    public ShoppingList getShoppingList() {
        ArrayList<ShoppingItem> items = new ArrayList<>();
        items.add(new ShoppingItem("Banány"));
        items.add(new ShoppingItem("Jahody"));
        items.add(new ShoppingItem("Brambory"));
        items.add(new ShoppingItem("Cibule"));
        items.add(new ShoppingItem("Červená cibule"));

        return new ShoppingList(items);
    }
}
