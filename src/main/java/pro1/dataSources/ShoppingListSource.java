package pro1.dataSources;

import pro1.dataModel.ShoppingList;

public interface ShoppingListSource {

    ShoppingList getShoppingList();

    // TODO 6
    // Základní verze tohoto rozhraní bude mít jednu metodu
    // s názvem getShoppingList, která nemá parametry
    // a vrací objekt typu ShoppingList

    // TODO 7
    // Přidejte do tohoto balíčku (složky) třídu
    // RandomShoppingListSource, která
    // implementuje rozhraní ShoppingListSource tak,
    // že vygeneruje jakýkoli nákupní seznam
    // s alespoň pěti různými položkami

    // TODO 8
    // Do třídy ShoppingListTableModel přidej metodu setModel
    // s parametrem typu ShoppingList.
    // V této metodě je nutné:
    // 1) Uložit parametr jako instanční proměnnou
    // 2) volat fireTableDataChanged();
    // Uprav ostatní metody třídy ShoppingListTableModel tak, aby
    // tabulka obsahovala všechna data z nákupního seznamu.

    // TODO 9
    // Do metody ShoppingMainFrame.createControls přidej zkušební kód, který:
    // 1) Vyvoří objekt typu RandomShoppingListSource
    // 2) Získá z něj objekt typu ShoppingList voláním metody getShoppingList
    // 3) Na dříve vytvořeném objektu tableModel1 zavolá metodu setModel

    // TODO 10
    // Zkontroluj, že aplikace zobrazuje tabulku s příslušnými
    // různými řádky a dvěma sloupci (textový popis, koupeno-ano/ne)
}
