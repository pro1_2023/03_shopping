package pro1;

import pro1.userInterface.ShoppingMainFrame;

import javax.swing.*;

public class Main
{
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(
            ()->{
                new ShoppingMainFrame();
            });
// Alternativně:
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                new ShoppingMainFrame();
//            }
//        });

        System.out.println("Program byl spusten");
    }

}

