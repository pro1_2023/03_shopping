package pro1.dataModel;

import java.util.ArrayList;

public class ShoppingList {
    ArrayList<ShoppingItem> polozky;

    public ShoppingList(ArrayList<ShoppingItem> polozky) {
        this.polozky = polozky;
    }

    public ArrayList<ShoppingItem> getPolozky() {
        return polozky;
    }

    public void setPolozky(ArrayList<ShoppingItem> polozky) {
        this.polozky = polozky;
    }

    // TODO 5
    // Základní verze této třídy bude jen obalem pro ArrayList<ShoppingItem>
    // Tento seznam bude k dispozici prostřednictvím getteru a setteru
    // Tento seznam bude také parametrem konstruktoru
}
