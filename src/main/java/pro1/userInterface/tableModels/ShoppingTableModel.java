package pro1.userInterface.tableModels;

import pro1.dataModel.ShoppingList;

import javax.swing.table.AbstractTableModel;

public class ShoppingTableModel extends AbstractTableModel {

    private ShoppingList data;

    public ShoppingList getModel() {
        return data;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if(columnIndex == 0)
        {
            return String.class;
        }
        else
        {
            return Boolean.class;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if(columnIndex == 0)
        {
            data.getPolozky().get(rowIndex).setNazev((String) aValue);
        }
        else if(columnIndex == 1)
        {
            data.getPolozky().get(rowIndex).setKoupeno((Boolean)aValue);
        }
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    public void setModel(ShoppingList data)
    {
        this.data = data;
        fireTableDataChanged();
    }

    public int getRowCount() {
        if(data == null)
        {
            return 0;
        }
        return data.getPolozky().size();
    }

    public int getColumnCount() {
        return 2;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if(columnIndex == 0)
        {
            return data.getPolozky().get(rowIndex).getNazev();
        }
        else if(columnIndex == 1)
        {
            return data.getPolozky().get(rowIndex).isKoupeno();
        }

        return null;
    }
    // TODO 1
    // Doplň tuto třídu tak, aby dědila od abstraktní třídy AbstractTableModel

    // TODO 2
    // Doplň tuto třídu tak, aby reprezentovala tabulku s
    // 20 řádky a 20 sloupci, naplněnou nulami

    // TODO 3
    // Doplň kód na konec metody ShoppingMainFrame.createControls,
    // aby se zobrazila tabulka s tímto table modelem
    // Nápověda:
    // ShoppingTableModel tableModel1 = new ShoppingTableModel();
    // JTable table1 = new JTable(tableModel1);
    // shoppingPanel.add(new JScrollPane(table1));

}
