package pro1.userInterface;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import pro1.dataModel.ShoppingList;
import pro1.dataSources.RandomShoppingListSource;
import pro1.userInterface.tableModels.ShoppingTableModel;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class Utils {
    public static void save(ShoppingTableModel tableModel, Component dialogParent) {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(dialogParent) == JFileChooser.APPROVE_OPTION) {
            try {
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String json = gson.toJson(tableModel.getModel());
                FileWriter fileWriter = new FileWriter(fileChooser.getSelectedFile());
                fileWriter.write(json);
                fileWriter.close();
            } catch (FileNotFoundException e) {
                JOptionPane.showMessageDialog(
                        dialogParent,
                        "Soubor nelze uložit" + "!",
                        "Chyba",
                        JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void open(ShoppingTableModel tableModel, Component dialogParent) {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(dialogParent) == JFileChooser.APPROVE_OPTION) {
            try {
                FileInputStream stream = new FileInputStream(fileChooser.getSelectedFile());
                System.out.println(fileChooser.getSelectedFile());
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                ShoppingList list = gson.fromJson(new InputStreamReader(stream), ShoppingList.class);
                tableModel.setModel(list);
                stream.close();
            } catch (FileNotFoundException e) {
                JOptionPane.showMessageDialog(
                        dialogParent,
                        "Soubor nelze otevřít" + "!",
                        "Chyba",
                        JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public static void create(ShoppingTableModel tableModel) {
        RandomShoppingListSource source = new RandomShoppingListSource();
        ShoppingList list = source.getShoppingList();
        tableModel.setModel(list);
    }
}
