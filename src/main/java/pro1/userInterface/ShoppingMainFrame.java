package pro1.userInterface;

import pro1.dataModel.ShoppingItem;
import pro1.dataModel.ShoppingList;
import pro1.dataSources.RandomShoppingListSource;
import pro1.userInterface.tableModels.ShoppingTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShoppingMainFrame extends JFrame
{
    ShoppingTableModel tableModel1;

    public ShoppingMainFrame()
    {
        createControls();
    }

    private void createControls()
    {
        setTitle("PRO1 Shopping");
        setVisible(true);
        setBackground(Color.white);
        setSize(600, 800);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JPanel shoppingPanel = new JPanel();
        shoppingPanel.setLayout(new BoxLayout(shoppingPanel,BoxLayout.Y_AXIS));
        this.add(shoppingPanel);

        shoppingPanel.add(createToolbar());

        JButton plusButton = new JButton("+");
        shoppingPanel.add(plusButton);

        JTextField textField = new JTextField();
        shoppingPanel.add(textField);
        textField.setMaximumSize(
                new Dimension(150,40)
        );
        plusButton.addActionListener(x -> {
            ShoppingItem newItem = new ShoppingItem(textField.getText());
            tableModel1.getModel().getPolozky().add(newItem);

            tableModel1.fireTableDataChanged();
        });

        tableModel1 = new ShoppingTableModel();
        JTable table1 = new JTable(tableModel1);
        shoppingPanel.add(new JScrollPane(table1));

        tableModel1.addTableModelListener(e -> {
            // REAGUJ NA ZMENU
        });
    }

    private JToolBar createToolbar()
    {
        JToolBar toolbar = new JToolBar("Tools",JToolBar.HORIZONTAL);
        toolbar.add(new AbstractAction("New", new ImageIcon(getClass().getResource("/new.gif"))) {
            public void actionPerformed(ActionEvent arg0) {
                Utils.create(tableModel1);
            }
        });
        toolbar.add(new AbstractAction("Open", new ImageIcon(getClass().getResource("/open.gif"))) {
            public void actionPerformed(ActionEvent arg0) {
                Utils.open(tableModel1, ShoppingMainFrame.this);
            }
        });
        toolbar.add(new AbstractAction("SaveAction", new ImageIcon(getClass().getResource("/save.gif"))) {
            public void actionPerformed(ActionEvent arg0) {
                Utils.save(tableModel1, ShoppingMainFrame.this);
            }
        });
        return toolbar;
    }

}
