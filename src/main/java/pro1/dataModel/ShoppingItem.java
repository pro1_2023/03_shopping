package pro1.dataModel;

public class ShoppingItem {
    String nazev;
    boolean koupeno = false;

    public ShoppingItem(String nazev) {
        this.nazev = nazev;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public boolean isKoupeno() {
        return koupeno;
    }

    public void setKoupeno(boolean koupeno) {
        this.koupeno = koupeno;
    }


    // Integer / int // Boolean / boolean

    // TODO 4
    // Základní verze této třídy bude obsahovat textový popis položky
    // a informaci zda byla už koupena (ano/ne)
    // Oba atributy budou k dispozici prostřednictvím getteru a setteru
    // Parametrem konstruktoru bude pouze textový popis položky
}
